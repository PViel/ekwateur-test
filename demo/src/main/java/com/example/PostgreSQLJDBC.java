package com.example;

import java.util.Random;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

import org.postgresql.util.PSQLException;


public class PostgreSQLJDBC {
    private static Random rand = new Random();

    public static void startDB(String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres");
            System.out.println("Opened database successfully"); 
            stmt = c.createStatement();

            drop(stmt);

            create(stmt);

            populatePart(stmt);

            populatePro(stmt);

            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
    }

    public static String addNewPartClient(String civilite, String name, String surname) {
        System.out.println("Try to insert new client part...");
        Integer id = getNextId();
        String ref = "EKW";
        int i = 8 - Integer.toString(id).length();
        String eightzeroes = "00000000";
        ref += eightzeroes.substring(0, i) + Integer.toString(id);
        String sql = String.format("INSERT INTO Client (id, ref, gas, elec, civilite, name, surname) " +
            "VALUES (%d, '%s', %d, %d, '%s', '%s', '%s');", id, ref, rand.nextInt(20, 100), rand.nextInt(100, 200), civilite, name, surname);
        
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres");
            System.out.println("Opened database successfully"); 
            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
            System.out.println("Insert part client successfully...");
            return ref;
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return null;
    }

    public static String addNewProClient(String siret, String raisonSoc, Integer ca) {
        System.out.println("Try to insert new client pro...");
        Integer id = getNextId();
        id++;
        String ref = "EKW";
        int i = 8 - Integer.toString(id).length();
        String eightzeroes = "00000000";
        ref += eightzeroes.substring(0, i) + Integer.toString(id);
        String sql = String.format("INSERT INTO Client (id, ref, gas, elec, siret, raisonsoc, ca) " +
            "VALUES (%d, '%s', %d, %d, '%s', '%s', %d);", id, ref, rand.nextInt(40, 200), rand.nextInt(150, 500), siret, raisonSoc, ca);
        
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres");
            System.out.println("Opened database successfully"); 
            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
            System.out.println("Insert pro client successfully...");
            return ref;
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return null;
    }

    private static Integer getNextId() {
        String sql = String.format("SELECT id FROM Client ORDER BY id DESC LIMIT 1");
        
        Connection c = null;
        Statement stmt = null;

        Integer id = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres"); 
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                id = rs.getInt("id");
            }
            stmt.close();
            c.close();
            return id;
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return null;
    }

    public static Integer getGas(String ref) {
        System.out.println("Try to select gas from " + ref + "...");
        String sql = String.format("SELECT gas FROM Client WHERE ref = '%s'", ref);
        
        Connection c = null;
        Statement stmt = null;

        Integer gas = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres");
            System.out.println("Opened database successfully"); 
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                gas = rs.getInt("gas");
            }
            stmt.close();
            c.close();
            System.out.println("Select gas successful...");
            return gas;
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return null;
    }

    public static Integer getElec(String ref) {
        System.out.println("Try to select elec from " + ref + "...");
        String sql = String.format("SELECT elec FROM Client WHERE ref = '%s'", ref);
        
        Connection c = null;
        Statement stmt = null;

        Integer elec = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres");
            System.out.println("Opened database successfully"); 
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                elec = rs.getInt("elec");
            }

            stmt.close();
            c.close();
            System.out.println("Select elec successful...");
            return elec;
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return null;
    }

    public static boolean checkRef(String ref) {
        System.out.println("Check if " + ref + " exists...");
        String sql = String.format("SELECT ref FROM Client WHERE ref = '%s'", ref);
        
        Connection c = null;
        Statement stmt = null;

        boolean exists = false;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "postgres"); 
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                exists = true;
                System.out.println(ref + " exists...");
            }

            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
        return exists;
    }

    private static void drop(Statement stmt) {
        System.out.println("Try to drop table client...");
        String sql = "DROP TABLE client";
        try {
            stmt.executeUpdate(sql);
            System.out.println("Database dropped successfully...");
        } catch (PSQLException e) {
            System.out.println(e.getClass().getName()+": "+ e.getMessage());
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
    }

    private static void create(Statement stmt) {
        System.out.println("Try to create table client...");
        String sql = "CREATE TABLE Client (" +
            " ID INT PRIMARY KEY     NOT NULL, " +
            " REF            TEXT    NOT NULL, " +
            " GAS            INT, " +
            " ELEC           INT, " +
            " SIRET          TEXT     DEFAULT NULL, " +
            " RAISONSOC      TEXT    DEFAULT NULL, " +
            " CA             INT     DEFAULT NULL, " +
            " CIVILITE       TEXT    DEFAULT NULL, " +
            " NAME           TEXT    DEFAULT NULL, " +
            " SURNAME        TEXT    DEFAULT NULL);";
        try {
            stmt.executeUpdate(sql);
            System.out.println("Table created successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
    }

    private static void populatePart(Statement stmt) {
        System.out.println("Try to populate table client with part...");
        String sql = "INSERT INTO Client (id, ref, gas, elec, civilite, name, surname) " +
            "VALUES (0, 'EKW00000000', 39, 168, 'M', 'Michael', 'Scott'), " +
            "(1, 'EKW00000001', 46, 176, 'M', 'Stanley', 'Hudson'), " +
            "(2, 'EKW00000002', 0, 0, 'M', 'Creed', 'Bratton'), " +
            "(3, 'EKW00000003', 81, 157, 'Mme', 'Phyllis', 'Vance'), " +
            "(4, 'EKW00000004', 76, 156, 'M', 'Darryl', 'Philbin'), " +
            "(5, 'EKW00000005', 58, 156, 'Mme', 'Kelly', 'Kapoor'), " +
            "(6, 'EKW00000006', 47, 186, 'M', 'Kevin', 'Malone'), " +
            "(7, 'EKW00000007', 38, 190, 'Mme', 'Angela', 'Martin');";
        try {
            stmt.executeUpdate(sql);
            System.out.println("Table populated successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
    }

    private static void populatePro(Statement stmt) {
        System.out.println("Try to populate table client with pro...");
        String sql = "INSERT INTO Client (id, ref, gas, elec, siret, raisonsoc, ca) " +
            "VALUES (8, 'EKW00000008', 78, 336, '00000000000000', 'Dunder Mifflin Paper Company', 3908761), " +
            "(9, 'EKW00000009', 92, 312, '00000000000001', 'Vance Refrigeration', 49000127), " +
            "(10, 'EKW00000010', 10, 40, '00000000000002', 'The Michael Scott Paper Company', 150);";
        try {
            stmt.executeUpdate(sql);
            System.out.println("Table populated successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName()+": "+ e.getMessage());
            System.exit(0);
        }
    }
}