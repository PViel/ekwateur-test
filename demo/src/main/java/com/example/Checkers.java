package com.example;

import java.util.Arrays;

public class Checkers {

    private static final String REGEX = "[a-zA-Z]+";

    private Checkers(){}

    public static boolean checkRef(String ref) {
        return ref.substring(0, 3).equals("EKW")
        && ref.substring(3).length() == 8
        && !ref.substring(3).matches(REGEX);
    }

    public static boolean checkCivilite(String civilite) {
        return Arrays.asList("Mme", "M").contains(civilite);
    }

    public static boolean checkOnlyLetters(String s) {
        return s.matches(REGEX);
    }

    public static boolean checkSiret(String siret) {
        return siret.length() == 14;
    }
}
