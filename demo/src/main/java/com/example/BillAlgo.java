package com.example;

public class BillAlgo {
    
    private double partGasPrice;
    private double partElecPrice;
    private double proUGasPrice;
    private double proUElecPrice;
    private double proOGasPrice;
    private double proOElecPrice;

    private Integer caThreshold;


    public BillAlgo(double partGasPrice, double partElecPrice, double proUGasPrice, double proUElecPrice,
            double proOGasPrice, double proOElecPrice, Integer caThreshold) {
        this.partGasPrice = partGasPrice;
        this.partElecPrice = partElecPrice;
        this.proUGasPrice = proUGasPrice;
        this.proUElecPrice = proUElecPrice;
        this.proOGasPrice = proOGasPrice;
        this.proOElecPrice = proOElecPrice;
        this.caThreshold = caThreshold;
    }

    public Double computeBillT(String client) {
        return computeBillG(client) + computeBillE(client);
    }
    public Double computeBillG(String client) {
        Integer gas = PostgreSQLJDBC.getGas(client);
        if (gas != null) {
            return (double) (gas * partGasPrice);
        }
        return null;
    }
    public Double computeBillE(String client) {
        Integer elec = PostgreSQLJDBC.getElec(client);
        if (elec != null) {
            return (double) (elec * partElecPrice);
        }
        return null;
    }


    //Getters&Setters
    public double getPartGasPrice() {
        return partGasPrice;
    }
    public void setPartGasPrice(double partGasPrice) {
        this.partGasPrice = partGasPrice;
    }
    public double getPartElecPrice() {
        return partElecPrice;
    }
    public void setPartElecPrice(double partElecPrice) {
        this.partElecPrice = partElecPrice;
    }
    public double getProUGasPrice() {
        return proUGasPrice;
    }
    public void setProUGasPrice(double proUGasPrice) {
        this.proUGasPrice = proUGasPrice;
    }
    public double getProUElecPrice() {
        return proUElecPrice;
    }
    public void setProUElecPrice(double proUElecPrice) {
        this.proUElecPrice = proUElecPrice;
    }
    public double getProOGasPrice() {
        return proOGasPrice;
    }
    public void setProOGasPrice(double proOGasPrice) {
        this.proOGasPrice = proOGasPrice;
    }
    public double getProOElecPrice() {
        return proOElecPrice;
    }
    public void setProOElecPrice(double proOElecPrice) {
        this.proOElecPrice = proOElecPrice;
    }
    public Integer getCaThreshold() {
        return caThreshold;
    }
    public void setCaThreshold(Integer caThreshold) {
        this.caThreshold = caThreshold;
    }
}
