package com.example;

import java.util.Scanner;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        PostgreSQLJDBC.startDB(args);
        Scanner sc= new Scanner(System.in);
        BillAlgo ba = new BillAlgo(0.012, 0.011, 0.021, 0.022, 0.023, 0.024, 1000000);
        String s = "";

        while (true) {
            System.out.println("Would you like to add a new client 'add' or check a bill 'bill': ");
            s = sc.nextLine();
            if ("add".equals(s)) {
                addPrompt(sc);
            } else if ("bill".equals(s)) {
                billPrompt(sc, ba);
            } else if ("exit".equals(s) || "kill".equals(s)) {
                break;
            } else {
                System.out.println("It's either 'add' or 'bill', nothing else: ");
            }
        }
    }

    private static void billPrompt(Scanner sc, BillAlgo ba) {
        while (true) {
            System.out.println("Enter ref client: ");
            String s = sc.nextLine();
            if (!PostgreSQLJDBC.checkRef(s)) {
                System.out.println("Error. Ref doesn't exists, please type again: ");
            } else if ("exit".equals(s) || "kill".equals(s)) {
                break;
            } else {
                System.out.println("Your bill is: " + Double.toString(ba.computeBillT(s)));
                break;
            }
        }
    }

    private static void addPrompt(Scanner sc) {
        System.out.println("Trying to add new client. Are you Pro (Y/n): ");
        String s = sc.nextLine();
        if ("".equals(s) || "y".equals(s)) {
            addPro(sc);
        } else if ("n".equals(s)) {
            addPart(sc);
        }
    }

    private static void addPro(Scanner sc) {
        String siret = "";
        String raisonSoc = "";
        Integer ca = 0;
        String s = null;
        while (true) {
            System.out.println("Please enter SIRET: ");
            s = sc.nextLine();
            if ("exit".equals(s) || "kill".equals(s)) {
                System.out.println("Stop adding new client.");
                return;
            } else if (!Checkers.checkSiret(s)) {
                System.out.println("Error in SIRET, please type again: ");
            } else {
                siret = s;
                while (true) {
                    System.out.println("Please enter Rasion Sociale: ");
                    s = sc.nextLine();
                    if ("exit".equals(s) || "kill".equals(s)) {
                        System.out.println("Stop adding new client.");
                        return;
                    } else {
                        raisonSoc = s;
                        while (true) {
                            System.out.println("Please enter CA: ");
                            s = sc.nextLine();
                            if ("exit".equals(s) || "kill".equals(s)) {
                                System.out.println("Stop adding new client.");
                                return;
                            } else if (s.matches("[a-zA-Z]+")){
                                System.out.println("NaN CA, please type again: ");
                            } else {
                                ca = Integer.parseInt(s);
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        String ref = PostgreSQLJDBC.addNewProClient(siret, raisonSoc, ca);
        System.out.println("Your ref is: " + ref);
    }

    private static void addPart(Scanner sc) {
        String civilite = "";
        String name = "";
        String surname = "";
        String s = null;
        while (true) {
            System.out.println("Please enter Civilite: ");
            s = sc.nextLine();
            if ("exit".equals(s) || "kill".equals(s)) {
                System.out.println("Stop adding new client.");
                return;
            } else if (!Checkers.checkCivilite(s)) {
                System.out.println("Error in Civilite, please type again: ");
            } else {
                civilite = s;
                while (true) {
                    System.out.println("Please enter Name: ");
                    s = sc.nextLine();
                    if ("exit".equals(s) || "kill".equals(s)) {
                        System.out.println("Stop adding new client.");
                        return;
                    } else if (!Checkers.checkOnlyLetters(s)){
                        System.out.println("Name with not just letters, please type again: ");
                    } else {
                        name = s;
                        while (true) {
                            System.out.println("Please enter Surname: ");
                            s = sc.nextLine();
                            if ("exit".equals(s) || "kill".equals(s)) {
                                System.out.println("Stop adding new client.");
                                return;
                            } else if (!Checkers.checkOnlyLetters(s)){
                                System.out.println("Surname with not just letters, please type again: ");
                            } else {
                                surname = s;
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        String ref = PostgreSQLJDBC.addNewPartClient(civilite, name, surname);
        System.out.println("Your ref is: " + ref);
    }
}
